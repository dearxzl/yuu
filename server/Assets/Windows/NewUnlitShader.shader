﻿Shader "Unlit/NewUnlitShader"
{
	Properties
	{
		_MainTex("Main Tex",2D) = ""{}
		_Int("Int Value",Int) = 1
		_Float("Float Value",Float)=1.5
		_Range("Range Value",Range(0,10))=2

		_Color("Color Value",Color)=(1,1,0,1)
		_Vector("Vector Value",Vector)=(1,1,1,1)

		_Tex("Tex Value",2D) = "white"{}
		_3DTex("3DTex Value",3D) = "black"{}
		_CubeTex("CubeTex Value",Cube) = "black"{}
	}

	Fallback "VertexLit"

}
