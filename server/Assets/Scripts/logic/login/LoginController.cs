﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoginController : MonoBehaviour 
{
	
	void Awake()
	{
		Debug.Log ("LoginController Init");
		Net.ServerSocket.RegisterCallbak (EnmCmdID.CsLoginReq, OnMessageCallback);		
	}

	void OnMessageCallback(byte[] bydata,System.Net.Sockets.Socket soc)
	{
        
        Loom.QueueOnMainThread(() =>
        {
            CSLoginReq reqObj = CSLoginReq.Parser.ParseFrom(bydata);
            if (reqObj != null)
            {
                Debug.Log("User Name:" + reqObj.LoginInfo.UserName);
                Debug.Log("PassWord:" + reqObj.LoginInfo.Password);
                Toast.Show(this, "用户尝试登录：\n 用户名是:" + reqObj.LoginInfo.UserName + "\n密码是：" + reqObj.LoginInfo.Password,
                    2,
                    Toast.Type.MESSAGE);
            }
            

        }
        );

        CSLoginReq reqObj2 = CSLoginReq.Parser.ParseFrom(bydata);
        CSLoginRes res = new CSLoginRes();

        if (reqObj2.LoginInfo.Password == "123")
        {
            res.ResultCode = 0;
        }
        else
        {
            res.ResultCode = 1;
        }

        Net.ServerSocket.Instance.SendPBData(soc, (int)EnmCmdID.CsLoginRes, res);
    }



	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
