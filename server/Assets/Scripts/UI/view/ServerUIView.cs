﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Net;

public class ServerUIView : MonoBehaviour 
{
	public Button mbtnBeginServer, mbtnStopServer;

	public void OnBegonServerClk()
	{
		Debug.Log ("Begin Server Clk");
		ServerSocket.Instance.BeginServer ();
		mbtnBeginServer.gameObject.SetActive (false);
		mbtnStopServer.gameObject.SetActive (true);
        Toast.Show(this, "开启服务器成功",
                2,
                Toast.Type.MESSAGE);
    }

	public void OnStopServerClk()
	{
		Debug.Log ("Stop Server Clk");
		ServerSocket.Instance.StopServer ();
		mbtnBeginServer.gameObject.SetActive (true);
		mbtnStopServer.gameObject.SetActive (false);
        Toast.Show(this, "关闭服务器成功",
                2,
                Toast.Type.MESSAGE);
    }

	void Awake()
	{
		Debug.Log ("Awake UI");
		mbtnBeginServer.gameObject.SetActive (true);
		mbtnStopServer.gameObject.SetActive (false);
	}
	
}