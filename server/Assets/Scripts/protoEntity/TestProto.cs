﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Google.Protobuf;

public class TestProto : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Person p = new Person ();
		p.Id = 1;
		p.Email = "dearxzl@sina.cn";
		p.Friends = "test";
		p.Name = "hi";

		int sizeLen = p.CalculateSize ();
		Debug.Log ("Proto Size:" + sizeLen);

		byte[] bydata = new byte[sizeLen];
		var stream = new CodedOutputStream (bydata);
		p.WriteTo (stream);

		Person p2 = Person.Parser.ParseFrom (bydata);

		Debug.Log ("Name:" + p2.Name);
		Debug.Log ("Email:" + p2.Email);
		Debug.Log ("Friends:" + p2.Friends);
		Debug.Log ("Id:" + p2.Id);

		CSLoginInfo info = new CSLoginInfo ();
		info.UserName = "1222";
		info.Password = "12222233332";
		CSLoginReq req = new CSLoginReq ();
		req.LoginInfo = info;

		byte [] pbdata = req.ToByteArray ();
		CSLoginReq req2 = CSLoginReq.Parser.ParseFrom (pbdata);
		Debug.Log ("UserName:" + req2.LoginInfo.UserName);
		Debug.Log ("Password:" + req2.LoginInfo.Password);

	}

}
