﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Collections.Generic;
//using System.Linq;
using System.Net.Sockets;
using System.Text;
//using System.Threading.Tasks;
using System.Net;
using System.Threading;
using Net;
using System.IO;
using Google.Protobuf;

namespace Net
{
	public class ServerSocket:SingletonTemplate<ServerSocket>
	{
		private static byte[] result = new byte[1024];
		private const int port = 8088;
		private static string IpStr = "0.0.0.0";
		private static Socket serverSocket;
		private static Thread mthlClientConnect;
		static List<Thread> mthlClientReceiveMessage=new List<Thread>();
        static List<Socket> mlstClientSocket = new List<Socket>();

        public void BeginServer()
		{
			IPAddress ip = IPAddress.Parse(IpStr);
			IPEndPoint ip_end_point = new IPEndPoint(ip, port);
			//创建服务器Socket对象，并设置相关属性
			serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			//绑定ip和端口
			serverSocket.Bind(ip_end_point);
			//设置最长的连接请求队列长度
			serverSocket.Listen(10);

			Debug.Log ("启动监听成功");//, serverSocket.LocalEndPoint.ToString());
			//在新线程中监听客户端的连接
			mthlClientConnect = new Thread(ClientConnectListen);
			mthlClientConnect.Start();
		}

		/// <summary>
		/// 客户端连接请求监听
		/// </summary>
		private static void ClientConnectListen()
		{
			while (true)
			{
				//为新的客户端连接创建一个Socket对象
				Socket clientSocket = serverSocket.Accept();
				Console.WriteLine("客户端{0}成功连接", clientSocket.RemoteEndPoint.ToString());
				//向连接的客户端发送连接成功的数据
//				ByteBuffer buffer = new ByteBuffer();
//				buffer.WriteString("Connected Server");
//				clientSocket.Send(WriteMessage(buffer.ToBytes()));
				//每个客户端连接创建一个线程来接受该客户端发送的消息
				Thread thl = new Thread(RecieveMessage);
				thl.Start(clientSocket);

				mthlClientReceiveMessage.Add (thl);
                mlstClientSocket.Add(clientSocket);

            }
			mthlClientConnect = null;
		}

		/// <summary>
		/// 数据转换，网络发送需要两部分数据，一是数据长度，二是主体数据
		/// </summary>
		/// <param name="message"></param>
		/// <returns></returns>
		private static byte[] WriteMessage(byte[] message)
		{
			MemoryStream ms = null;
			using (ms = new MemoryStream())
			{
				ms.Position = 0;
				BinaryWriter writer = new BinaryWriter(ms);
				ushort msglen = (ushort)message.Length;
				writer.Write(msglen);
				writer.Write(message);
				writer.Flush();
				return ms.ToArray();
			}
		}

		/// <summary>
		/// 接收指定客户端Socket的消息
		/// </summary>
		/// <param name="clientSocket"></param>
		private static void RecieveMessage(object clientSocket)
		{
			Socket mClientSocket = (Socket)clientSocket;
			while (true)
			{
				try
				{
					int receiveNumber = mClientSocket.Receive(result,sizeof(int),SocketFlags.None);
                    if (receiveNumber == 0)
                    {
                        Debug.Log("Client Is Close");
                        mClientSocket.Shutdown(SocketShutdown.Both);
                        mClientSocket.Close();
                        mthlClientReceiveMessage.Remove(Thread.CurrentThread);
                        mlstClientSocket.Remove(mClientSocket);
                        Loom.QueueOnMainThread(() =>
                        {
                            Toast.Show(ServerSocket.Instance, "客户端断开", 1, Toast.Type.ERROR);
                        }
                        );
                        break;
                    }
//					Console.WriteLine("接收客户端{0}消息， 长度为{1}", mClientSocket.RemoteEndPoint.ToString(), receiveNumber);
					ByteBuffer buff = new ByteBuffer(result);
					//数据长度
					int len = buff.ReadInt();
					//数据内容
					receiveNumber = mClientSocket.Receive(result,len,SocketFlags.None);
					buff = new ByteBuffer(result);
					int nCmdID = buff.ReadInt();
					Debug.Log("Cmd ID:"+nCmdID);
					EnmCmdID cid = (EnmCmdID)nCmdID;
                    if (mdictMessageCB.ContainsKey(cid))
                    {
                        mdictMessageCB[cid]( buff.ReadBytesWithSpecLen(len-sizeof(int)),mClientSocket);
                    }
                   

                }
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
					mClientSocket.Shutdown(SocketShutdown.Both);
					mClientSocket.Close();
					mthlClientReceiveMessage.Remove (Thread.CurrentThread);
                    mlstClientSocket.Remove(mClientSocket);
                    break;
				}
			}
		}

		void OnDestroy()
		{
			StopServer ();
		}


		public void StopServer()
		{
			if (mthlClientConnect != null) 
			{
				mthlClientConnect.Abort ();	
				mthlClientConnect = null;

			}
			if (mthlClientReceiveMessage != null) 
			{
				for (int i = 0; i < mthlClientReceiveMessage.Count; i++) {
					if (mthlClientReceiveMessage [i] != null) {
						mthlClientReceiveMessage [i].Abort ();
					}
                    
				}

				mthlClientReceiveMessage.Clear ();

			}
            for (int i = 0; i < mlstClientSocket.Count; i++)
            {
                if (mlstClientSocket[i] != null&&mlstClientSocket[i].Connected)
                {
                    mlstClientSocket[i].Shutdown(SocketShutdown.Both);
                    mlstClientSocket[i].Close();
                }
            }
            mlstClientSocket.Clear();

            if (serverSocket != null) {
				serverSocket.Close ();
			}
		}

		static Dictionary<EnmCmdID,System.Action<byte[],Socket> > mdictMessageCB = new Dictionary<EnmCmdID, Action<byte[], Socket>>();
		static public void RegisterCallbak(EnmCmdID eci,System.Action<byte[],Socket> datatoreg)
		{
			mdictMessageCB [eci] = datatoreg;
		}

        public void SendMessage(Socket soc, byte[] data)
        {
            try
            {
                ByteBuffer buffer = new ByteBuffer();
                buffer.WriteBytes(data);
                soc.Send(buffer.ToBytes());
            }
            catch
            {
                soc.Shutdown(SocketShutdown.Both);
                soc.Close();
            }
        }


        public void SendPBData(Socket soc,  int typeID, IMessage sndData)
        {
            Debug.Log("Cmd ID:" + typeID);

            byte[] byData = sndData.ToByteArray();
            byte[] byPBData = CreatePBData(typeID, byData);
            SendMessage( soc, byPBData);
        }

        private byte[] CreatePBData(int typeId, byte[] pbuf)
        {
            byte[] pbdata = pbuf;
            ByteBuffer buff = new ByteBuffer();
            buff.WriteInt(typeId);
            buff.WriteBytesWithoutLen(pbdata);
            return buff.ToBytes();
        }


    }


}