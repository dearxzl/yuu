﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Collections.Generic;
//using System.Linq;
using System.Net.Sockets;
using System.Text;
//using System.Threading.Tasks;
using System.Net;
using System.Threading;
using Net;
using System.IO;

public class SingletonTemplate<T> : MonoBehaviour where T : MonoBehaviour
{
	private static volatile T instance;
	private static object syncRoot = new System.Object();
	public static T Instance
	{
		get
		{
			if (instance == null)
			{
				lock (syncRoot)
				{
					if (instance == null)
					{
						T[] instances = FindObjectsOfType<T>();
						if (instances != null)
						{
							for (var i = 0; i < instances.Length; i++)
							{
								Destroy(instances[i].gameObject);
							}
						}
						GameObject go = new GameObject();
						go.name = typeof(T).Name;
						instance = go.AddComponent<T>();
						DontDestroyOnLoad(go);
					}
				}
			}
			return instance;
		}
	}
}